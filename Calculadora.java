import java.util.Scanner;

public class Calculadora {
    public static void main(String[] args) {
        System.out.println("CALCULADORA SIMPLES");

        int opcao;
        do {

            System.out.println("1- Somar");
            System.out.println("2- Subtrair");
            System.out.println("3- Multiplicar");
            System.out.println("4- Dividir");
            System.out.print("O que você deseja fazer ? (0 para sair): ");

            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();
            processar(opcao);
        } while   (opcao != 0);
    }
        
        public static void processar(int opcao) {
            switch(opcao) {
                case 1: {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("SOMANDO DOIS NÚMEROS");

                    System.out.print("Digite o primeiro número: ");
                    int numero1 = scanner.nextInt();

                    System.out.print("Digite o Segundo número: ");
                    int numero2 = scanner.nextInt();
                    int soma = numero1 + numero2;

                    System.out.println("A soma dos dois números é: " + soma);
                    break;
                }

                case 2:{
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("SUBTRAINDO DOIS NÚMEROS");

                    System.out.print("Digite o primeiro número: ");
                    int numero1 = scanner.nextInt();

                    System.out.print("Digite o Segundo número: ");
                    int numero2 = scanner.nextInt();
                    int subtrair = numero1 - numero2;

                    System.out.println("A subtração dos dois números é: " + subtrair);
                    break;
                }

                case 3:{

                    Scanner scanner = new Scanner(System.in);
                    System.out.println("MULTIPLICANDO DOIS NÚMEROS");

                    System.out.print("Digite o primeiro número: ");
                    int numero1 = scanner.nextInt();

                    System.out.print("Digite o Segundo número: ");
                    int numero2 = scanner.nextInt();
                    int multiplicar = numero1 * numero2;

                    System.out.println("A soma dos dois números é: " + multiplicar);
                    break;
                }

                case 4:{

                    Scanner scanner = new Scanner(System.in);
                    System.out.println("DIVIDINDO DOIS NÚMEROS");

                    System.out.print("Digite o primeiro número: ");
                    double numero1 = scanner.nextInt();

                    System.out.print("Digite o Segundo número: ");
                    double numero2 = scanner.nextInt();
                    double divisão = numero1 / numero2;

                        if (numero2 == 0) {
                            System.out.println("Impossível dividir por zero !");
                        }  else {
                            System.out.println("O resultado da divisão dos dois números é: " + divisão);
                    break;
                        }
                }
            }
        }
   
}